extern crate ndarray;
extern crate openblas_src;

use argmin::core::observers::{ObserverMode, SlogLogger};
use argmin::core::{ArgminError, CostFunction, Error, Executor, Gradient};
use ndarray::parallel::par_azip;
use ndarray::prelude::*;
use ndarray_rand::RandomExt;
use ndarray_stats::QuantileExt;
use numpy::{IntoPyArray, PyArray1, PyArray2, PyReadonlyArray2};
use pyo3::prelude::*;
use rand_core::*;
use rand_distr::Normal;
use rand_pcg::Pcg64Mcg;

use argmin::solver::linesearch::MoreThuenteLineSearch;
use argmin::solver::quasinewton::BFGS;
use argmin::solver::quasinewton::LBFGS;

// Define the epsilon of the distance^2 for the energy regularisation at short distances
const EPS_DIST2: f32 = f32::EPSILON;

/// The main UMAP class.
/// Only the overall parameters are defined here (not the actual dataset).
#[pyclass]
struct UMAP {
    /// b parameter, note that the a parameter is not defined as it only rescales the whole
    /// dimensionality reduction.
    b_par: f32,
    /// sigma for the v_ij interaction term.
    /// In this implementation, we suppose it is the same for all points.
    sigma: f32,
    /// vij_bi is defined when using two clusters of equal size with inter and intra interaction
    /// terms. This is mainly for theoretical purposes. If defined, the two remaining paramters are
    /// not used.
    vij_bi: Option<(f32, f32)>,
    /// vij_uni is defined when using two clusters of equal size with inter and intra interaction
    /// terms. This is also mainly for theoretical purposes. If defined, the rho parameter is not
    /// used.
    vij_uni: Option<f32>,
    /// rho_uni is defined when using a global (uniform) minimal distance between points.
    rho_uni: Option<f32>,
}

#[pymethods]
impl UMAP {
    #[new]
    fn new(
        b_par: f32,
        sigma: f32,
        vij_bi: Option<(f32, f32)>,
        vij_uni: Option<f32>,
        rho_uni: Option<f32>,
    ) -> Self {
        UMAP {
            b_par,
            sigma,
            vij_bi,
            vij_uni,
            rho_uni,
        }
    }

    #[args(max_iter = "1000")]
    fn fit_transform<'py>(
        &self,
        py: Python<'py>,
        xin: PyReadonlyArray2<f32>,
        yinit: PyReadonlyArray2<f64>,
        max_iter: u64,
    ) -> PyResult<&'py PyArray2<f64>> {
        let feats = xin.as_array().to_owned();
        let nobs = feats.nrows();

        // The following in the not the most efficient way to do it but constant v_ij initialization
        // if very particular anyway (mostly to inspect the algorithm).
        let vij = match self.vij_bi {
            Some(inter_intra) => {
                let mut vij_mut = Array::zeros((nobs, nobs));
                vij_mut
                    .slice_mut(s![..nobs / 2, ..nobs / 2])
                    .fill(inter_intra.0);
                vij_mut
                    .slice_mut(s![nobs / 2.., nobs / 2..])
                    .fill(inter_intra.0);
                vij_mut
                    .slice_mut(s![..nobs / 2, nobs / 2..])
                    .fill(inter_intra.1);
                vij_mut
                    .slice_mut(s![nobs / 2.., ..nobs / 2])
                    .fill(inter_intra.1);
                vij_mut
            }
            None => match self.vij_uni {
                Some(val) => Array::from_elem((nobs, nobs), val),
                None => comp_vij(&feats, self.sigma, self.rho_uni).unwrap(),
            },
        };
        // Define cost function

        let b_par = self.b_par;
        let cost = UmapPb { b_par, vij };

        let init_pos = yinit.as_array().to_owned();
        println!("initial position array shape: {:?}", init_pos.dim());

        // set up a line search
        // This uses the defaults values for the parameters but it should be improved.
        let linesearch = MoreThuenteLineSearch::new()
            .with_bounds(1e-15, 10.0)
            .unwrap();

        // Set up solver The solver needs to have a number of vectors it keeps to represent the
        // hessian. I don't know how this number should scale with the size of the problem??
        // Intuitively, I would make it scale linearly with nobs, but I am not sure this is the
        // correct choice...
        let nvec = 32;
        let solver = LBFGS::new(linesearch, nvec)
            .with_tolerance_grad(1e-4)
            .unwrap()
            .with_tolerance_cost(1e-4)
            .unwrap();

        let res = Executor::new(cost, solver)
            .configure(|state| {
                state
                    .param(init_pos.into_shape((2 * nobs,)).unwrap())
                    .max_iters(max_iter)
            })
            .add_observer(SlogLogger::term(), ObserverMode::Every(16))
            .run()
            .unwrap();

        // Print result
        let paramout = res.state().best_param.to_owned();
        match paramout {
            Some(expr) => {
                println!("{:?}", expr);
                let nel = expr.len();
                Ok(expr.into_shape((nel / 2, 2)).unwrap().into_pyarray(py))
            }
            None => Ok(Array::<f64, _>::zeros((0, 0)).into_pyarray(py)),
        }
    }

    fn comp_rho<'py>(
        &self,
        py: Python<'py>,
        xin: PyReadonlyArray2<f32>,
    ) -> PyResult<&'py PyArray1<f32>> {
        let feats = xin.as_array().to_owned();

        let rho = comp_rho(&feats).unwrap();
        Ok(rho.into_pyarray(py))
    }

    fn comp_vij<'py>(
        &self,
        py: Python<'py>,
        xin: PyReadonlyArray2<f32>,
    ) -> PyResult<&'py PyArray2<f32>> {
        let feats = xin.as_array().to_owned();
        let nobs = feats.nrows();

        // The following is not the most efficient way to do it but constant v_ij initialization
        // it is very particular anyway (mostly to inspect the algorithm).
        let vij = match self.vij_uni {
            Some(val) => Array::from_elem((nobs, nobs), val),
            None => comp_vij(&feats, self.sigma, self.rho_uni).unwrap(),
        };
        Ok(vij.into_pyarray(py))
    }
}

fn draw_sample_othogonal_noise(
    n_samp: usize,
    sigma_noise: f32,
    signal_max: f32,
    dim: usize,
    mut rng: impl RngCore,
) -> Array2<f32> {
    let signal = Array::linspace(0.0f32, signal_max, n_samp);
    let mut vec = Array::random_using(
        (n_samp, dim),
        Normal::new(0.0, sigma_noise).unwrap(),
        &mut rng,
    );
    let mut first_row = vec.index_axis_mut(Axis(1), 0);
    first_row += &signal;

    return vec;
}

struct UmapPb {
    b_par: f32,
    vij: Array2<f32>,
}

fn cost_di_vi(dij: &f32, vij: &f32, b_par: f32) -> f32 {
    let distreg = (EPS_DIST2 * EPS_DIST2 + dij * EPS_DIST2 + dij * dij) / (EPS_DIST2 + dij);
    (1f32 + dij.powf(b_par)).ln() - b_par * (1f32 - vij) * distreg.ln()
}

impl CostFunction for UmapPb {
    type Param = Array1<f64>;
    type Output = f64;

    fn cost(&self, pos: &Self::Param) -> Result<Self::Output, Error> {
        let nel = pos.len();
        let posresh = pos.mapv(|x| x as f32).into_shape((nel / 2, 2)).unwrap();
        let mut distm = distmatrix(&posresh);
        distm.diag_mut().fill(1f32);
        let mut costs = Array::<f64, _>::zeros(nel / 2);
        par_azip!((co in &mut costs, di in distm.rows(), vi in self.vij.rows()) {
            azip!((di_i in di, vi_i in vi) {
                let cost32 = cost_di_vi(di_i, vi_i, self.b_par);
                *co += cost32 as f64;
            }
            );
        });
        let cost = costs.sum();
        Ok(cost)
    }
}

impl Gradient for UmapPb {
    type Param = Array1<f64>;
    type Gradient = Array1<f64>;

    fn gradient(&self, pos: &Self::Param) -> Result<Self::Gradient, Error> {
        let nel = pos.len();
        let posresh = pos.mapv(|x| x as f32).into_shape((nel / 2, 2)).unwrap();
        let mut distm = distmatrix(&posresh);
        distm.diag_mut().fill(1f32);

        par_azip!((di in &mut distm, vi in &self.vij) {
                let distb = di.powf(self.b_par);
                let numden1 = distb / *di / (1f32 + distb);
                let sh_di = EPS_DIST2 + *di;
                let grad_reglog = (sh_di + *di) / (sh_di * sh_di - EPS_DIST2 * *di) - 1f32 / sh_di;
                let numden2 = (1f32 - vi) * grad_reglog;
                *di = 4f32 * self.b_par * (numden1 - numden2);
            }
        );

        let mut grad = Array::<f64, _>::zeros((pos.len() / 2, 2));
        par_azip!((mut gra in grad.rows_mut(), po in posresh.rows(), pre in distm.rows()) {
                let prefbroad = pre.into_shape((pre.len(), 1)).unwrap();
                let mularr = ((po.to_owned().into_shape((1, 2)).unwrap() - &posresh) * prefbroad)
                    .mapv(|x| x as f64)
                    .sum_axis(Axis(0));
                gra.assign(&mularr);
            }
        );

        let gradflat = grad.into_shape(pos.len()).unwrap();
        let minmaxclamp = 1e3;
        Ok(gradflat.mapv(|x| x.clamp(-minmaxclamp, minmaxclamp)))
    }
}

fn distmatrix(points: &Array2<f32>) -> Array2<f32> {
    // initialize array
    let mut distances = Array::<f32, _>::zeros((points.nrows(), points.nrows()));
    let dim = points.ncols();
    par_azip!((mut di in distances.rows_mut(), po in points.rows()) {
            let mut diff = po.to_owned().into_shape((1, dim)).unwrap() - points;
            diff.mapv_inplace(|x| x * x);
            let sumdiff = diff.sum_axis(Axis(1));
            di.assign(&sumdiff);
        }
    );

    distances
}

fn expterm(xin: &mut ArrayViewMut1<f32>, rho: f32, sigma: f32) {
    xin.mapv_inplace(|xinp| ((rho - xinp.sqrt()) / sigma).exp());
}

fn comp_rho(pos_ini: &Array2<f32>) -> Result<Array1<f32>, Error> {
    let mut dmat = distmatrix(pos_ini);
    println!("distance matrix shape: {:?}", dmat.dim());

    let large = 1e15f32;
    dmat.diag_mut().fill(large);

    let mut rho = Array::<f32, _>::zeros((dmat.nrows(),));

    par_azip!((rh in &mut rho, row in dmat.rows()) *rh = row.min().unwrap().sqrt());

    Ok(rho)
}

fn comp_vij(pos_ini: &Array2<f32>, sigma: f32, rho_uni: Option<f32>) -> Result<Array2<f32>, Error> {
    let mut dmat = distmatrix(pos_ini);
    println!("distance matrix shape: {:?}", dmat.dim());

    let large = 1e15f32;
    dmat.diag_mut().fill(large);

    match rho_uni {
        Some(val) => {
            par_azip!((mut dm in dmat.rows_mut()) expterm(&mut dm, val, sigma));

            // We now symmetrize the matrix by taking v_ij + v_ji - v_ij * v_ji
            par_azip!((dm in &mut dmat) *dm = 2f32 * *dm - *dm * *dm);
        }
        None => {
            let mut rho = Array::<f32, _>::zeros((dmat.nrows(),));

            par_azip!((rh in &mut rho, row in dmat.rows()) *rh = row.min().unwrap().sqrt());
            par_azip!((mut dm in dmat.rows_mut(), rh in &rho) expterm(&mut dm, *rh, sigma));

            let dmat_cp = dmat.clone();

            // We now symmetrize the matrix by taking v_ij + v_ji - v_ij * v_ji
            par_azip!((dm in &mut dmat, dm_t in dmat_cp.t()) *dm = *dm + *dm_t - *dm * *dm_t);
        }
    };

    // Set the diagonal to 1 to avoid overflow
    dmat.diag_mut().fill(1f32);

    Ok(dmat)
}

fn optimize_andinit(
    feats: &mut Array2<f32>,
    noise_level: f64,
    mut rng: impl RngCore,
) -> Result<Array2<f64>, Error> {
    // Define cost function
    let vij = comp_vij(&feats, 0.01f32, None).unwrap();

    let nobs = feats.nrows();

    let cost = UmapPb { b_par: 0.95, vij };

    println!("Compute the init pos.");
    let init_xpos: Array1<f64> = Array::<f64, _>::linspace(0., 1. / noise_level / 5., nobs);
    let init_ypos = Array::random_using((nobs,), Normal::new(0.0, noise_level).unwrap(), &mut rng);
    println!("init xpos shape: {:?}", init_xpos.dim());
    let mut init_pos = Array::<f64, _>::zeros((nobs, 2));
    println!("init pos shape: {:?}", init_pos.dim());
    init_pos.slice_mut(s![.., 0]).assign(&init_xpos);
    init_pos.slice_mut(s![.., 1]).assign(&init_ypos);
    println!("init pos shape: {:?}", init_pos.dim());
    let init_hessian: Array2<f64> = Array2::eye(2 * nobs);

    // set up a line search
    let linesearch = MoreThuenteLineSearch::new().with_c(1e-4, 0.9)?;

    // Set up solver, this is the older version that uses BFGS rather than LBFGS
    let solver = BFGS::new(linesearch);

    let res = Executor::new(cost, solver)
        .configure(|state| {
            state
                .param(init_pos.into_shape((2 * nobs,)).unwrap())
                .inv_hessian(init_hessian)
                .max_iters(10)
        })
        .add_observer(SlogLogger::term(), ObserverMode::Every(16))
        .run()?;

    // Print result
    let paramout = res.state().best_param.to_owned();
    match paramout {
        Some(expr) => {
            println!("{:?}", expr);
            let nel = expr.len();
            Ok(expr.into_shape((nel / 2, 2)).unwrap())
        }
        None => Err(Error::new(ArgminError::NotInitialized {
            text: String::from("The optimization has not occured yet"),
        })),
    }
}

/// Build a random array
#[pyfunction]
fn rand_array<'py>(py: Python<'py>) -> &'py PyArray2<f64> {
    // Get a seeded random number generator for reproducibility (Isaac64 algorithm)
    let seed = 42u64;
    let mut rng = Pcg64Mcg::seed_from_u64(seed);

    let noise_level = 0.01f64;
    let dim_ini = 64;
    let n_samp = 4096;
    let mut arr = draw_sample_othogonal_noise(n_samp, noise_level as f32, 1.0, dim_ini, &mut rng);
    println!("Features shape: {:?}", arr.dim());
    let umap_dimred = optimize_andinit(&mut arr, noise_level, &mut rng).unwrap();
    umap_dimred.into_pyarray(py)
}

/// This module implements UMAP in Rust.
#[pymodule]
fn umaprs(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<UMAP>()?;
    m.add_function(wrap_pyfunction!(rand_array, m)?)?;
    Ok(())
}
