"""Test the UMAP rust implementation."""
import logging

import numpy as np
from matplotlib import pyplot as plt

import umaprs


def test_umaprs():
    """Start the main test."""
    logging.basicConfig(level=logging.INFO)
    nobs = 32
    dim = 16
    noise = 0.02

    rng = np.random.default_rng(42)
    xin = noise * rng.standard_normal((nobs, dim)).astype(np.float32)
    xin[:, 0] += np.linspace(0, 1, nobs)

    yinit = np.zeros((nobs, 2))
    yinit[:, 0] += np.linspace(0, nobs, nobs)

    b_par = 1.0
    sigma = 8 / nobs
    umap = umaprs.UMAP(b_par=b_par, sigma=sigma)
    outarr = umap.fit_transform(xin, yinit, max_iter=800)

    fig, ax = plt.subplots()
    ax.scatter(outarr[:, 0], outarr[:, 1])
    plt.show()

    assert outarr.shape[0] == nobs
    assert outarr.shape[1] == 2


def test_purenoise():
    """Start the main test."""
    logging.basicConfig(level=logging.INFO)
    nobs = 64
    dim = 16

    rng = np.random.default_rng(42)
    xin = rng.standard_normal((nobs, dim)).astype(np.float32)

    yinit = rng.standard_normal((nobs, 2))

    b_par = 1.0
    sigma = 8 * dim**0.25
    umap = umaprs.UMAP(b_par=b_par, sigma=sigma, vij_uni=0.9)
    outarr = umap.fit_transform(xin, yinit, max_iter=800)

    fig, ax = plt.subplots()
    ax.scatter(outarr[:, 0], outarr[:, 1])
    plt.show()

    assert outarr.shape[0] == nobs
    assert outarr.shape[1] == 2
